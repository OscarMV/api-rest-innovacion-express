import { Router, Request, Response } from 'express';
import { getRepository } from 'typeorm';
import * as jwt from 'jsonwebtoken';

const router:Router = Router();
import User from '../models/User';
import Token from '../models/Token';

// Middlewares
import tokenValidation from '../middlewares/verifyToken';

router.post('/api/signin', async(_req:Request, _res:Response) => {
    console.log(_req.body);
    const userRepository = getRepository(User);
    const tokenRepository = getRepository(Token);
    try{
        const newUser = new User;
        newUser.email = _req.body.email;
        newUser.password = _req.body.password;
        newUser.name = _req.body.name;
        const { id:userId } = await userRepository.save(newUser);

        const userToken = new Token();
        userToken.token = jwt.sign({
            data: userId
        }, process.env.SERVER_TOKEN || '', { expiresIn: '1h' });
        await tokenRepository.save(userToken);

        newUser.token = userToken;
        await userRepository.save(newUser);

        _res.status(200)
        .header({
            'token': userToken.token
        })
        .json({
            'serverMessage': 'User registrered succesfully'
        })
    }catch(e){
        if(e.code === '23505') _res.status(400).json({
            'serverMesssage': 'email already registrered'
        });
        console.log(e);
        _res.status(500).json({
            'serverMessage': 'Something goes really really bad :c'
        });
    }
});

router.post('/api/login', async(_req:Request, _res:Response) => {
    const userRepository = getRepository(User);
    const tokenRepository = getRepository(Token);
    const user = await userRepository.findOne({
        where: {email: _req.body.email},
        relations: ["token"]
    });
    
    if(user === undefined) _res.status(200).json({
        'serverMessage': 'User not found'
    });
    if(user?.password !== _req.body.password) _res.status(200).json({
        'serverMessage': 'Invalid password'
    });

    const oldUserToken = user?.token.id;

    const userToken = new Token();
    userToken.token = jwt.sign({
        data: user?.id
    }, process.env.SERVER_TOKEN || '', { expiresIn: '1h' });
    await tokenRepository.save(userToken);

    user.token = userToken;
    await userRepository.save(user);

    await tokenRepository.delete(oldUserToken);

    _res.status(200)
    .header({
        'token': userToken.token
    })
    .json({
        'serverMessage': 'login'
    });
});

router.post('/api/logout', tokenValidation, async(_req:Request, _response:Response) => {
    const tokenRepository = getRepository(Token);
    try{
        let updateToken = await tokenRepository.findOne({
            where: {id: _req.tokenId}
        });
        updateToken.active = false;
        await tokenRepository.save(updateToken);

        _response.status(200).json({
            'serverMessage': 'logout'
        });
    }
    catch(e){
        console.log(e);
        _response.status(500).json({
            'serverMessage': 'Something goes really bad :c'
        })
    }
});

export default router;