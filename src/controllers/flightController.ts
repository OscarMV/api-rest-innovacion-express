import { Router, Request, Response } from 'express';
import { getRepository } from 'typeorm';

import Flight from '../models/Flight'

const router:Router = Router();

// Middlewares
import tokenValidation from '../middlewares/verifyToken';

router.post('/api/flight', async(_req:Request, _res:Response) => {
    const flightRepository = getRepository(Flight);
    try{
        const newFlight = new Flight;
        newFlight.destiny = _req.body.destiny;
        newFlight.date = _req.body.date;
        newFlight.cost = _req.body.cost;
        newFlight.placesAvailable = _req.body.placesAvailable;

        await flightRepository.save(newFlight);

        _res.status(200).json({
            'serverMessage': 'Flight created succesfully'
        });
    }catch(e){
        console.log(e);
        _res.status(500).json({
            'serverMessage': 'Internal server error'
        });
    }
    


});

router.get('/api/flight', tokenValidation, async(_req:Request, _res:Response) => {
    const flightRepository = getRepository(Flight);
    const flights = await flightRepository.find();
    _res.status(200).json(flights);
});

export default router;