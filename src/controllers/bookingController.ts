import { Router, Request, Response } from 'express';
import { getRepository } from 'typeorm';

import Flight from '../models/Flight';
import User from '../models/User';
import Booking from '../models/Booking';

const router:Router = Router();

// Middlewares
import tokenValidation from '../middlewares/verifyToken';

router.post('/api/booking', tokenValidation,async(_req:Request, _res:Response) => {
    const userRepository = getRepository(User);
    const flightRepository = getRepository(Flight);
    const bookingRepository = getRepository(Booking);

    const updateFlight = await flightRepository.findOne({
        where: {id: _req.body.flightId}
    });

    console.log(updateFlight);

    if(updateFlight === undefined) _res.status(401).json({
        'serverMessage': 'Flight not found'
    });
    if(updateFlight.placesAvailable < _req.body.bookedPlaces) _res.status(401).json({
        'serverMessage': 'Not enough places'
    });

    console.log(_req.userId);
    const user = await userRepository.findOne({
        where: {id: _req.userId}
    });

    console.log(user);

    const newBooking = new Booking;
    newBooking.bookedPlaces = _req.body.bookedPlaces;
    newBooking.user = user;
    newBooking.flight = updateFlight;

    console.log(newBooking);

    await bookingRepository.save(newBooking);
    updateFlight.placesAvailable -= newBooking.bookedPlaces;
    await flightRepository.save(updateFlight);

    console.log(updateFlight);
    _res.status(200).json({
        'serverMessage': 'Booking created succesfully'
    });
});

router.get('/api/booking', tokenValidation, async(_req:Request, _res:Response) => {
    const bookingRepository = getRepository(Booking);
    const reservedFlights = await bookingRepository.find({
        where: {userId: _req.userId}
    });
    console.log(reservedFlights);
    _res.status(200).json(reservedFlights);
});

router.put('/api/booking', tokenValidation, async(_req:Request, _res:Response) => {
    console.log(_req.body);
    const bookingRepository = getRepository(Booking);
    const flightRepository = getRepository(Flight);
    const updateBooking = await bookingRepository.findOne({
        where: {id: _req.body.bookingId},
        relations: ['flight']
    });


    const updateFlight = updateBooking.flight;
    updateFlight.placesAvailable += updateBooking.bookedPlaces - _req.body.bookedPlaces;

    console.log(updateFlight.placesAvailable);
    console.log(updateBooking?.bookedPlaces);
    console.log(_req.body.bookedPlaces);

    updateBooking.bookedPlaces = _req.body.bookedPlaces;

    await bookingRepository.save(updateBooking);
    await flightRepository.save(updateFlight);

    _res.status(200).json({
        'serverMessage': 'Booking updated succesfully'
    });
});

router.delete('/api/booking', tokenValidation, async(_req:Request, _res:Response) => {
    console.log(_req.body);
    const bookingRepository = getRepository(Booking);
    const flightRepository = getRepository(Flight);
    const updateBooking = await bookingRepository.findOne({
        where: {id: _req.body.bookingId},
        relations: ['flight']
    });


    const updateFlight = updateBooking.flight;
    updateFlight.placesAvailable += updateBooking.bookedPlaces;

    await flightRepository.save(updateFlight);

    await bookingRepository.delete({
        where: {id: _req.body.flightId}
    });

    _res.status(200).json({
        'serverMessage': 'Booking deleted succesfully'
    });
})

export default router;