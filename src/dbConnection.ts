import { createConnection } from 'typeorm';

const dbConnection = (async() => {
    const dbConfig = {
        type: process.env.DB_TYPE,
        host: process.env.DB_HOST,
        port: process.env.DB_PORT,
        username: process.env.DB_USER,
        password: process.env.DB_PASS,
        database: process.env.DB_NAME,
        synchronize: true,
        entities: [__dirname + '/models/*.js']
    };
    await createConnection(dbConfig);
});

export default dbConnection;