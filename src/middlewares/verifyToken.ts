import { Request, Response, NextFunction } from 'express';
import * as jwt from 'jsonwebtoken';
import { getRepository } from 'typeorm';

import User from '../models/User';

interface IPayload {
    data: number,
    iat: number,
    exp: number
}

export const tokenValidation = async (_req: Request, _res: Response, _next: NextFunction) => {
    const usertoken = _req.header('token');
    const userRepository = getRepository(User);
    if(!usertoken) return _res.status(401).json({
        'serverMessage': 'Acces denied'
    });
    try{
        const payload = jwt.verify(usertoken, process.env.SERVER_TOKEN || '') as IPayload;

        const { token } = await userRepository.findOne({
            select: ['id', 'token'],
            where: {id: payload.data},
            relations: ['token']
        });

        if(!token.active) return _res.status(401).json({
            'serverMessage': 'Corrupted token'
        });


        _req.userId = payload.data;
        _req.tokenId = token.id;
        _next();
    }catch(e){
        console.log(e);
        return _res.status(401).json({
            'serverMessage': 'Corrupted token'
        });
    }
};

export default tokenValidation;