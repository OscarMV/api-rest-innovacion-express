import { Entity, 
    PrimaryColumn, 
    Column, 
    BeforeInsert,
    ManyToOne} from 'typeorm';

import * as uuidv4 from 'uuid/v4';

import User from './User';
import Flight from './Flight';

@Entity({name: 'booking'})
export class Booking {
    @PrimaryColumn({
        type: 'uuid',
        unique: true
    })
    id: string;

    @Column({
        type: 'integer'
    })
    bookedPlaces: string;

    @ManyToOne(_type => User, user => user.bookings)
    user: User;

    @ManyToOne(_type => Flight, flight => flight.bookings)
    flight: Flight;

    @BeforeInsert()
    addId(){
        this.id = uuidv4();
    }
}

export default Booking;