import { Entity, 
    PrimaryColumn, 
    Column, 
    BeforeInsert,
    JoinColumn,
    OneToOne,
    OneToMany} from 'typeorm';

import * as uuidv4 from 'uuid/v4';

import Token from './Token';
import Booking from './Booking';

@Entity({name: 'person'})
export class User {
    @PrimaryColumn({
        type: 'uuid',
        unique: true
    })
    id: string;

    @Column({
        type: 'varchar',
        length: 80,
    })
    name: string;

    @Column({
        type: 'varchar',
        length: 50,
        unique: true
    })
    email: string;

    @Column({
        type: 'varchar',
        length: 40,
    })
    password: string;

    @OneToOne(_type => Token)
    @JoinColumn()
    token: Token;

    @OneToMany(_type => Booking, booking => booking.user)
    bookings: Booking[];

    @BeforeInsert()
    addId(){
        this.id = uuidv4();
    }
}

export default User;