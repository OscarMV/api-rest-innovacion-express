import { Entity, 
    PrimaryColumn, 
    Column, 
    BeforeInsert,
    OneToMany} from 'typeorm';

import * as uuidv4 from 'uuid/v4';

import Booking from './Booking';

@Entity({name: 'flight'})
export class Flight {
    @PrimaryColumn({
        type: 'uuid',
        unique: true
    })
    id: string;

    @Column({
        type: 'varchar',
        length: 90,
    })
    destiny: string;

    @Column({
        type: 'varchar',
        length: 50
    })
    cost: string;

    @Column({
        type: 'varchar',
        length: 50,
    })
    date: string;

    @Column({
        type: 'integer'
    })
    placesAvailable: number;

    @OneToMany(_type => Booking, booking => booking.flight)
    bookings: Booking[];

    @BeforeInsert()
    addId(){
        this.id = uuidv4();
    }
}

export default Flight;