import { Entity, 
    PrimaryColumn, 
    Column, 
    BeforeInsert} from 'typeorm';

import * as uuidv4 from 'uuid/v4';

@Entity({name: 'token'})
export class Token {
    @PrimaryColumn({
        type: 'uuid',
        unique: true
    })
    id: string;

    @Column({
        type: 'boolean',
        default: true
    })
    active: boolean;

    @Column({
        type: 'varchar',
        length: 200,
    })
    token: string;

    @BeforeInsert()
    addId(){
        this.id = uuidv4();
    }
}

export default Token;