import * as express from 'express';
import * as morgan from 'morgan';
import * as dotenv from 'dotenv';

import dbConnection from './dbConnection';

import sessionController from './controllers/sessionController';
import flightController from './controllers/flightController';
import bookingController from './controllers/bookingController';

const server = express();
dotenv.config();

server.set('port', process.env.SERVER_PORT || 8080);

server.use(morgan('dev'));
server.use(express.json());

// Controllers
server.use(sessionController);
server.use(flightController);
server.use(bookingController);

server.listen(server.get('port'), async()=> {
    console.log(`Server on port ${server.get('port')}`);
    await dbConnection();
});