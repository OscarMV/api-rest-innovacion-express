declare namespace Express {
    interface Request {
        userId: string;
        tokenId: string;
    }
}